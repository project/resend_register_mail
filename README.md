# Resend registration / welcome email

This module adds an action to resend the registration / welcome email for
selected user accounts.

This is a contrib version of the work being done in the following Drupal core
patch:
[drupal.org patch](https://www.drupal.org/i/5688)

For a full description of the module, visit the
[project page](https://www.drupal.org/project/resend_register_mail).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/resend_register_mail).


## Requirements

This module requires the Drupal core 'user' module.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

There is no configuration provided.


## Maintainers

- Sven Decabooter - [svendecabooter](https://www.drupal.org/u/svendecabooter)
